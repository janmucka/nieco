//Tom je zlatokop z Clondyke. Rad by si kupil parcelu, na ktorej by mohol tazit zlato. Celá mapa CLondyke sa dá nakresliť ako obdĺžnik, rozdeleny na X x Y štvorcov (tzv. rajonov). Všetky rajóny majú rovnaké rozmery. Podľa pravidiel CFZ (Clondajkska federacia zlatokopov) musí každý kúpený pozemok spĺňať nasledujúce podmienky: - najmenšou nedeliteľnou časťou je rajon. - každý pozemok musí byť štvorec. - kúpna cena pozemku závisí od počtu jeho rajónov (1 rajon stojí presne A$). Tom by chcel zarobiť čo najviac, preto sa rozhodol, že si dá záležať na výbere svojej parcely. Každý rajón na mape ohodnotil jedným celým číslom = cenou zlata, ktore sa na ňom nachádza. Napíšte program, ktorý prečíta Tomovú mapu a najde vhodný pozemom s pravidlami CFZ, z ktorého by tom vedel zarobiť čo najviac. (Teda tom vyťaží toľko zlata, koľko ho odhadol na svojej mape, avšak musí ešte zaplatiť za kúpený pozemok). Prvý riadok vstupu bude obsahovať jediné číslo N, 1<=N<=100. Ďalej bude nasledovať prázdny riadok a po ňom N sád vstupu oddelených takisto prázdnym riadkom. Každá sada vstupu bude začítať riadkom, v ktorom 3 čísla X,Y,A (1<=X<=100 - označuje počet riadkov na Tomovej mape, 1<=Y<=100 - označuje počet stĺpcov na Tomovej mape, 1<=A<=1000 = kupna cena jedného rajónu). V nasledujúcich X riadkoch bude Y nezáporných celých čísiel <= 1000, ktorými je popísaná Tomová mapa. Pre každú sadu vstupu má program vypísať jeden riadok tvaru: "Najvyhodnejsi pozemok ma zisk X." , kde X je najvyžší možný zisk z nejakého pozemku po zaplatení nákupnej ceny podľa pravidiel CFZ. V prípade, že sa na mape nedá nič zarobiť program má vypísať správu: "Na ziadnom pozemku sa neda zarobiť!"

#include<stdio.h>
#define MAXLEN 101

int main(){
	int N,X,Y,A,n,x,y,Z,C;
	int mapa[MAXLEN][MAXLEN];
	int v=0,k,l,i,j,h,g,e,V;
	scanf("%d",&N); //načíta koľko polí budem vyhodnocovať
	for(n=1;n<=N;n++){ //pre každé pole spraví nasledovné
		scanf("%d%d%d",&X,&Y,&A); //načíta X riadkov Y stĺpcov A náklady
		for(x=0; x < X; x++){ //načíta jednotlivé čísla do riadkov a stĺpcov
			for(y=0; y < Y; y++)
				scanf("%d",&mapa[x][y]);
		}
		for(g=0;g<X;g++){ // 
			for(h=0;h<Y;h++){
				i=g;
				j=h;
				e=0;
				while(e<((X<Y)?X:Y)){ //zvacsovanie pola
					for(k=g;k<=i;k++){
						for(l=h;l<=j;l++){
							if(i>=X||j>=Y)
								continue;
							v+=mapa[k][l]-A;
						}
					}
					if(v>V)
						V=v;
					printf("%d g=%d h=%d i=%d j=%d e=%d V=%d\n",v,g,h,i,j,e,V);
					//printf("%d\n",v);
					v=0;
					i++;
					j++;
					e++;
				}
			}
		}
		if(V>0)
			printf("Najvyhodnejsi pozemok ma zisk %d.\n",V);
		else
			printf("Na ziadnom pozemku sa neda zarobit!\n");
		V=0;
	}
	return 0;
}

